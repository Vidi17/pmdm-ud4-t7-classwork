package dam.android.vidal.u4t7preferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

public class U4T7PreferencesActivity extends AppCompatActivity {

    private final String MYPREFS = "MyPrefs";
    private EditText etPlayerName, etScore;
    private Spinner spinnerLevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u4_t7_preferences);

        setUI();
    }

    private void setUI(){
        etPlayerName = findViewById(R.id.etPlayerName);

        // Level spinner, set adapter from string-array resource
        spinnerLevel = findViewById(R.id.spinnerLevel);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this
                , R.array.levels, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(spinnerAdapter);

        etScore = findViewById(R.id.etScore);

        Button btQuit = findViewById(R.id.btQuit);
        btQuit.setOnClickListener(v -> finish());
    }

    @Override
    protected void onPause() {
        super.onPause();

        // get preference file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        // save UI data to file
        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putInt("Level", spinnerLevel.getSelectedItemPosition());
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));

        editor.apply();
    }

    // read activity data from preferences file
    @Override
    protected void onResume() {
        super.onResume();

        // get preferences file
        SharedPreferences myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);

        // set UI values reading data from file
        etPlayerName.setText(myPreferences.getString("PlayerName", "unknown"));
        spinnerLevel.setSelection(myPreferences.getInt("Level", 0));
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));

    }
}