package dam.android.vidal.u4t7preferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class PreferencesSecondActivity extends AppCompatActivity {

    private SharedPreferences myPreferences;
    private TextView tvShowPrefs, tvPrefsName, tvPlayer, tvScore, tvLevel, tvDifficulty, tvSound
            , tvColour, tvPlayerResponse, tvScoreResponse, tvLevelResponse, tvDifficultyResponse
            , tvSoundResponse, tvColourResponse, tvShowing, tvFile;

    private String preferencesType, preferencesName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_preferences);
        preferencesType = getIntent().getStringExtra("PreferencesType");
        preferencesName = getIntent().getStringExtra("PreferencesName");

        setUI();
    }

    // TODO EX 2.2

    private void setUI(){
        tvColour = findViewById(R.id.tvColour);
        tvDifficulty = findViewById(R.id.tvDifficulty2);
        tvLevel = findViewById(R.id.tvLevel2);
        tvPlayer = findViewById(R.id.tvPlayer2);
        tvScore = findViewById(R.id.tvScore2);
        tvPrefsName = findViewById(R.id.tvPrefsName);
        tvShowPrefs = findViewById(R.id.tvShowPrefs);
        tvSound = findViewById(R.id.tvSound);
        tvPlayerResponse = findViewById(R.id.tvPlayerResponse);
        tvScoreResponse = findViewById(R.id.tvScoreResponse);
        tvLevelResponse = findViewById(R.id.tvLevelResponse);
        tvDifficultyResponse = findViewById(R.id.tvDifficultyResponse);
        tvSoundResponse = findViewById(R.id.tvSoundResponse);
        tvColourResponse = findViewById(R.id.tvColourResponse);
        tvFile = findViewById(R.id.tvFile);
        tvShowing = findViewById(R.id.tvShowing);

        choosePreferences();
        setTextViews();
    }

    // TODO EX 2.2

    private void choosePreferences(){
        switch (preferencesType){

            // TODO EX 3

            case "ENCRYPTED SHARED PREFS":
                try {
                    String mainKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
                    myPreferences = EncryptedSharedPreferences.create(
                            preferencesName,
                            mainKeyAlias,
                            getApplicationContext(),
                            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                    );
                } catch (GeneralSecurityException | IOException e) {
                    e.printStackTrace();
                }
                break;
            case "SHARED PREFS":
                myPreferences = getSharedPreferences(preferencesName, MODE_PRIVATE);
                break;
            case "PREFERENCES":
                myPreferences = getPreferences(MODE_PRIVATE);
                break;
            default:
                myPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                break;
        }
    }

    // TODO EX 2.2

    private void setTextViews(){
        tvShowing.setText(R.string.showing);
        tvShowPrefs.setText(preferencesType);
        tvFile.setText(R.string.file);
        tvPrefsName.setText(preferencesName);

        tvPlayer.setText(R.string.player);
        tvPlayerResponse.setText(myPreferences.getString("PlayerName", "unknown"));

        tvSound.setText(R.string.sound);
        tvSoundResponse.setText(String.valueOf(myPreferences.getBoolean("SoundActive", false)));

        tvColour.setText(R.string.colour);
        tvColourResponse.setText(String.valueOf(myPreferences.getInt("Colour", 0)));

        tvDifficulty.setText(R.string.tvDifficulty);
        tvDifficultyResponse.setText(String.valueOf(myPreferences.getInt("Difficulty", 0)));

        tvLevel.setText(R.string.level);
        tvLevelResponse.setText(String.valueOf(myPreferences.getInt("Level", 0)));

        tvScore.setText(R.string.score);
        tvScoreResponse.setText(String.valueOf(myPreferences.getInt("Score", 0)));
    }
}