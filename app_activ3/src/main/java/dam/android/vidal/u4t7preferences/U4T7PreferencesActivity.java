package dam.android.vidal.u4t7preferences;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.security.keystore.KeyGenParameterSpec;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class U4T7PreferencesActivity extends AppCompatActivity {

    // TODO EX 2.1
    /*
        ¿Cuál es el nombre del fichero de preferencias que se guarda?

         El nombre del fichero de preferencias que se guarda es MyPrefs.xml

        ¿Qué uso tiene (qué representa) ese fichero de preferencias?

        El fichero de preferencias es la manera estándar de almacenar una colección pequeña
        de pares clave-valor de una aplicación o conjunto de aplicaciones.
     */

    //TODO EX 1.1

    private final String MYPREFS = "MyPrefs";
    private SharedPreferences myPreferences;
    private MediaPlayer mp;
    private EditText etPlayerName, etScore;
    private CheckBox soundActive;
    private RadioGroup difficultyButtons;
    private RadioButton rbSharedPreferences, rbPreferences, rbDefaultSharedPreferences, rbEncryptedPreferences;
    private Spinner spinnerLevel, spColours;

    private String preferencesType = "SHARED PREFS";

    public U4T7PreferencesActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u4_t7_preferences);
        
        myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);
        mp = MediaPlayer.create(this, R.raw.notification_sound);

        setUI();
    }

    //TODO EX 1.1

    private void setUI(){
        etPlayerName = findViewById(R.id.etPlayerName);

        rbSharedPreferences = findViewById(R.id.rbSharedPreferences);
        rbPreferences = findViewById(R.id.rbPreferences);
        rbDefaultSharedPreferences = findViewById(R.id.rbDefaultSharedPreferences);
        rbEncryptedPreferences = findViewById(R.id.rbEncryptedPreferences);

        setRadioButtonsListeners();

        // Level spinner, set adapter from string-array resource
        spinnerLevel = findViewById(R.id.spinnerLevel);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this
                , R.array.levels, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLevel.setAdapter(spinnerAdapter);

        spColours = findViewById(R.id.spColours);
        ArrayAdapter<CharSequence> coloursAdapter = ArrayAdapter.createFromResource(this
                , R.array.colours, android.R.layout.simple_spinner_item);
        coloursAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spColours.setAdapter(coloursAdapter);



        spColours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                changeBackgroundColor(position, parent.getRootView());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        difficultyButtons = findViewById(R.id.difficultyButtons);

        soundActive = findViewById(R.id.cbSound);

        etScore = findViewById(R.id.etScore);

        Button btQuit = findViewById(R.id.btQuit);
        btQuit.setOnClickListener(v -> finish());
        Button btShowPreferences = findViewById(R.id.btShowPreferences);
        btShowPreferences.setOnClickListener(v -> {
            Intent intent = new Intent(getBaseContext(), PreferencesSecondActivity.class);
            if (soundActive.isChecked()){
                mp.start();
            }
            intent.putExtra("PreferencesType", preferencesType);
            intent.putExtra("PreferencesName", MYPREFS);
            startActivity(intent);
        });
    }

    // TODO EX 2.2

    private void setRadioButtonsListeners(){
        rbSharedPreferences.setOnClickListener(v -> {
            myPreferences = getSharedPreferences(MYPREFS, MODE_PRIVATE);
            preferencesType = "SHARED PREFS";
        });

        rbPreferences.setOnClickListener(v -> {
            myPreferences = getPreferences(MODE_PRIVATE);
            preferencesType = "PREFERENCES";
        });

        rbDefaultSharedPreferences.setOnClickListener(v -> {
            myPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            preferencesType = "DEFAULT SHARED PREFS";
        });

        // TODO EX 3

        rbEncryptedPreferences.setOnClickListener(v -> {
            KeyGenParameterSpec keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC;
            try {
                String mainKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec);
                myPreferences = EncryptedSharedPreferences.create(
                        MYPREFS,
                        mainKeyAlias,
                        getApplicationContext(),
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                );
            }catch (IOException | GeneralSecurityException ex){
                System.err.println(ex.getMessage());
            }
            preferencesType = "ENCRYPTED SHARED PREFS";
        });
    }

    //TODO EX 1.2

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences.Editor editor = myPreferences.edit();

        editor.putString("PlayerName", etPlayerName.getText().toString());
        editor.putBoolean("SoundActive", soundActive.isChecked());
        editor.putInt("Difficulty", difficultyButtons.getCheckedRadioButtonId());
        editor.putInt("Colour", spColours.getSelectedItemPosition() + 1);
        editor.putInt("Level", spinnerLevel.getSelectedItemPosition() + 1);
        editor.putInt("Score", Integer.parseInt(etScore.getText().toString()));

        editor.apply();
    }

    //TODO EX 1.2
    // read activity data from preferences file
    @Override
    protected void onResume() {
        super.onResume();

        int colourPosition = myPreferences.getInt("Colour", 0);

        // set UI values reading data from file
        etPlayerName.setText(myPreferences.getString("PlayerName", "unknown"));
        soundActive.setChecked(myPreferences.getBoolean("SoundActive", false));
        spColours.setSelection(colourPosition - 1);
        difficultyButtons.check(myPreferences.getInt("Difficulty", 0));
        spinnerLevel.setSelection(myPreferences.getInt("Level", 0) - 1);
        etScore.setText(String.valueOf(myPreferences.getInt("Score", 0)));

    }

    //TODO EX 1.2

    private void changeBackgroundColor(int colourPosition, View view) {
        Resources res = getResources();
        String[] colourValues = res.getStringArray(R.array.colourValues);

        view.setBackgroundColor(Color.parseColor(colourValues[colourPosition]));
    }
}